import store from '@/store'
import request from '@/utils/request'
import i18n from '@/i18n' // Internationalization

var regionList = [],
  personTags = [],
  contentTags = []
var menuList = [
  {
    id: 1,
    name: 'Tik Tok',
    imgUrl: require('@/img/tiktok.png'),
  },
  {
    id: 2,
    name: 'You Tube',
    imgUrl: require('@/img/YouTube.png'),
  },
  {
    id: 3,
    name: 'Instagram',
    imgUrl: require('@/img/Instagram.png'),
  },
  {
    id: 4,
    name: 'Facebook',
    imgUrl: require('@/img/Facebook.png'),
  },
  {
    id: 5,
    name: 'Twitter',
    imgUrl: require('@/img/Twitter.png'),
  },
  {
    id: 6,
    name: 'Twitch',
    imgUrl: require('@/img/Twitch.png'),
  },
  {
    id: 7,
    name: 'Other',
    imgUrl: require('@/img/other.png'),
  },
]
function queryRegion() {
  request({
    url: '/nca/common/region/query',
    method: 'post',
    data: {
      auditStatus: 1,
      // regionName: this.checkForm.keyword,
      // id: store.getters.userInfo.id,
      // objId:store.getters.userInfo.id,
      // objType:1
    },
  }).then((res) => {
    regionList = res.data
  })
}
function queryTagsP() {
  request({
    url: '/nca/common/tags/query',
    method: 'post',
    data: {
      auditStatus: 1,
      tagType: 0,
      // objId:store.getters.userInfo.id,
      // objType:1
    },
  }).then((res) => {
    personTags = res.data
  })
}
function queryTagsC() {
  request({
    url: '/nca/common/tags/query',
    method: 'post',
    data: {
      tagType: 1,
      auditStatus: 1,
      // objId:store.getters.userInfo.id,
      // objType:1
    },
  }).then((res) => {
    contentTags = res.data
  })
}
queryRegion()
queryTagsP()
queryTagsC()
export function getRegion(id) {
  if (id === null) return null
  let item = regionList.find((i) => {
    return i.id === id
  })
  return item
    ? i18n.locale === 'en'
      ? item.regionNameEn
      : item.regionName
    : ''
}
export function getTagsP(id) {
  if (id === null) return null
  let item = personTags.find((i) => {
    return i.id === id
  })
  return item ? (i18n.locale === 'en' ? item.tagNameEn : item.tagName) : ''
}
export function getTagsC(id) {
  if (id === null) return null
  let item = contentTags.find((i) => {
    return i.id === id
  })
  return item ? (i18n.locale === 'en' ? item.tagNameEn : item.tagName) : ''
}

export function getPlatformUrl(id) {
  if (id === null) return null
  let item = menuList.find((value) => {
    return (value.id = id)
  })
  return item.imgUrl
}
