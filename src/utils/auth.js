import Cookies from 'js-cookie'

const TokenKey = 'broker/token'
const UserKey = 'broker/user'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function setUserData(data) {
  let stringData = ''
  if (data) {
    stringData = JSON.stringify(data)
  }
  localStorage.setItem(UserKey, stringData)
}

export function removeUserData() {
  localStorage.removeItem(UserKey)
}

export function getUserData() {
  const userStringData = localStorage.getItem(UserKey)
  let userData
  if (typeof userStringData === 'string') {
    try {
      userData = JSON.parse(userStringData)
    } catch (error) {
      userData = undefined
    }
  }
  return userData
}
