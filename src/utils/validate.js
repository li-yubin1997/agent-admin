/**
 * Created by PanJiaChen on 16/11/18.
 */

/**
 * @param {string} path
 * @returns {Boolean}
 */
export function isExternal(path) {
  return /^(https?:|mailto:|tel:)/.test(path)
}

/**
 * @param {string} str
 * @returns {Boolean}
 */
export function validUsername(str) {
  const valid_map = ['admin', 'editor']
  return valid_map.indexOf(str.trim()) >= 0
}

/**
 * 手机号码格式正则
 */
export const phoneNumer = /^1[3456789]\d{9}$/

/**
 * 用户姓名格式正则
 *
 */
export const username = /^[\u4e00-\u9fa5·a-zA-Z0-9]+$/
