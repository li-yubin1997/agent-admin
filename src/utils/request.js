import axios from "axios";
import { message } from "@/utils/reset-message"; // reset message
import store from "@/store";
import { getToken } from "@/utils/auth";

const ErrorText = "网络出小差了，请稍后再试";

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 60000, // request timeout
});

// request interceptor
service.interceptors.request.use(
  (config) => {
    // do something before request is sent

    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers["token"] = getToken();
      config.headers["currentUserId"] = store.getters.userInfo.id;
      config.headers["userType"] = 1;
    }
    return config;
  },
  (error) => {
    // do something with request error
    console.error(error); // for debug
    return Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  (response) => {
    const res = JSON.parse(JSON.stringify(response.data));

    // if the custom code is not 20000, it is judged as an error.
    if (res.rcode !== 0) {
      message({
        message: res.rmsg || ErrorText,
        type: "error",
        duration: 2 * 1000,
        onClose: () => {
          // 403: Token expired;
          if (res.rcode === 403) {
            // to re-login
            // MessageBox.confirm('You have been logged out, you can cancel to stay on this page, or log in again', 'Confirm logout', {
            //   confirmButtonText: '重新登录',
            //   cancelButtonText: this.$t('取消'),
            //   type: 'warning'
            // }).then(() => {
            //   store.dispatch('user/resetToken').then(() => {
            //     location.reload()
            //   })
            // })
            // store.dispatch('user/resetToken').then(() => {
            //   location.reload()
            // })
          }
        },
      });

      return Promise.reject(res || "Error");
    } else {
      // 获取登录的token
      if (
        response.config.url === "/nca/broker-user/login" ||
        response.config.url === "/nca/admin-user/switch/broker-user"
      ) {
        res.data.token = response.headers.token;
      }
      return res;
    }
  },
  (error) => {
    console.error("err" + error); // for debug
    message({
      message: error.rmsg || ErrorText,
      type: "error",
      duration: 2 * 1000,
    });
    return Promise.reject(error);
  }
);

export default service;
