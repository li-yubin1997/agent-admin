import router from "./router";
import store from "./store";
import { Message } from "element-ui";
import NProgress from "nprogress"; // progress bar
import "nprogress/nprogress.css"; // progress bar style
import { getToken, getUserData } from "@/utils/auth"; // get token from cookie
import getPageTitle from "@/utils/get-page-title";
import i18n from "./i18n";

NProgress.configure({ showSpinner: false }); // NProgress Configuration

const whiteList = ["/login", "/auth-redirect", "/mobile", "/reset-password"]; // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start();

  // set page title
  document.title = i18n.locale === "zh" ? "KOL Manager" : "OKOL.VIP";
  // document.title = getPageTitle(to.meta.title)

  // 管理员跳转免登录
  if (to.query && to.query.switchId) {
    await store.dispatch("user/loginFree", to.query.switchId);
    to.query.switchId = undefined;
    next({ path: "/", replace: true });
  }

  // determine whether the user has logged in
  const hasToken = getToken();
  const hasUserInfo = getUserData();

  if (hasToken && hasUserInfo) {
    if (to.path === "/login" || to.path === "mobile") {
      // if is logged in, redirect to the home page
      next({ path: "/" });
      NProgress.done(); // hack: https://github.com/PanJiaChen/vue-element-admin/pull/2939
    } else {
      next();
      // determine whether the user has obtained his permission roles through getInfo
      const hasRoles = store.getters.roles && store.getters.roles.length > 0;
      if (hasRoles) {
        next();
      } else {
        try {
          // get user info
          // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
          // const { roles } = await store.dispatch('user/getInfo')
          const roles =
            store.getters.roles.length > 0 ? store.getters.roles : ["admin"];
          store.dispatch("user/setRoles", roles);

          // generate accessible routes map based on roles
          const accessRoutes = await store.dispatch(
            "permission/generateRoutes",
            roles
          );
          // dynamically add accessible routes
          router.addRoutes(accessRoutes);
          // console.log(accessRoutes)
          // hack method to ensure that addRoutes is complete
          // set the replace: true, so the navigation will not leave a history record
          next({ ...to, replace: true });
        } catch (error) {
          // remove token and go to login page to re-login
          await store.dispatch("user/resetToken");
          Message.error(error || "Has Error");
          // TODO 重新登录返回上一页的内容
          // next(`/login?redirect=${to.path}`)
          next("/login");
          NProgress.done();
        }
      }
    }
  } else {
    /* has no token*/
    console.log(to.path);
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next();
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`);
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  // finish progress bar
  NProgress.done();
});
