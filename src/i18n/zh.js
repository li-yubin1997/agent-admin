export default {
  common: {
    username: "用户名",
    password: "密码",
    save: "保存",
    edit: "编辑",
    update: "更新",
    delete: "删除",
    forever: "永久",
    expired: "过期",
  },
  剩余秒: "剩余{s}秒",
  经纪人页面: "KOL Manager",
};
