import Vue from "vue";
import Router from "vue-router";
import Layout from "@/layout";
import headerImg from "@/layout/headerImg";

Vue.use(Router);

/* Layout */
// import Layout from '@/layout'

/* Router Modules */
// import { DashboardRoutes } from './modules/dashboard'
// import { StatisticsRoutes } from './modules/statistics'
// import { AccountRoutes } from './modules/account'
import {
  ProjectRoutes,
  AddProjectRoutes,
  DetailProjectRoutes,
  AddNetCelebrityRoutes,
  CooperativeListRoutes,
} from "./modules/project";
import { TagsRoutes } from "./modules/tags";
// import { TaskRoutes } from './modules/task'
import { UserRoutes } from "./modules/userInfo";
import { FavoriteRoutes } from "./modules/favorite";
// import { OperatorRoutes } from './modules/operator'
// import { LogRoutes } from './modules/log'

const isMobile = /Mobile/i.test(navigator.userAgent);
console.log("isMobile", isMobile);

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true,
  },
  // {
  //   path: '/404',
  //   component: () => import('@/views/error-page/404'),
  //   hidden: true
  // },
  // {
  //   path: '/401',
  //   component: () => import('@/views/error-page/401'),
  //   hidden: true
  // },
  {
    path: "/",
    redirect: isMobile ? "/mobile" : "/dashboard", // 首页跳转
  },
  {
    path: "/change-password",
    component: () => import("@/views/change-password/index"),
    hidden: true,
  },
  {
    path: "/reset-password",
    component: () => import("@/views/reset-password/index"),
    hidden: true,
  },
  // DashboardRoutes,
  {
    path: "/",
    component: Layout,
    redirect: "/dashboard",
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: () => import("@/views/dashboard/index"),
        meta: { title: "达人管理", icon: "dashboard" },
      },
    ],
  },
  // TaskRoutes,
  ProjectRoutes,
  AddProjectRoutes,
  DetailProjectRoutes,
  AddNetCelebrityRoutes,
  TagsRoutes,
  UserRoutes,
  FavoriteRoutes,
  CooperativeListRoutes,
  {
    path: "/contact-platform",
    component: Layout,
    children: [
      {
        path: "contact-platform",
        name: "contact-platform",
        component: () => import("@/views/contact-platform/index"),
        meta: { title: "联系我们", icon: "contact" },
      },
    ],
  },
  {
    path: "/platform-brief",
    component: Layout,
    children: [
      {
        path: "platform-brief",
        name: "platform-brief",
        component: () => import("@/views/platform-brief/index"),
        meta: { title: "关于平台", icon: "dashboard" },
      },
    ],
  },

  // StatisticsRoutes,
  // AccountRoutes,
  // OperatorRoutes,
  // LogRoutes,

  // 404 page must be placed at the end !!!
  // { path: '*', redirect: '/404', hidden: true }
];

if (isMobile) {
  constantRoutes.push({
    path: "/mobile",
    component: () => import("@/views/mobile/login"),
    hidden: true,
  });
}

export const asyncRoutes = [];

const createRouter = () =>
  new Router({
    mode: "history", // require service support
    base: process.env.VUE_APP_PACKAGE,
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes,
  });

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
