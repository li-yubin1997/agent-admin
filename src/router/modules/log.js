/* Layout */
import Common from '@/layout/common'

export const LogRoutes = {
  path: '/log',
  component: Common,
  children: [{
    path: 'index',
    name: 'Log',
    component: () => import('@/views/log/index'),
    meta: { title: '系统日志', icon: 'side-log', affix: true }
  }]
}
