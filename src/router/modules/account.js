/* Layout */
import Common from '@/layout/common'

export const AccountRoutes = {
  path: '/account-manage',
  component: Common,
  redirect: '/account-manage/index',
  children: [{
    path: 'index',
    name: 'UserManage',
    component: () => import('@/views/account-manage/index'),
    meta: { title: '用户管理', icon: 'side-account-manage', affix: true }
  }]
}
