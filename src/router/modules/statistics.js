/* Layout */
import Common from '@/layout/common'

export const StatisticsRoutes = {
  path: '/statistics',
  component: Common,
  redirect: '/statistics/devices',
  name: 'Statistics',
  meta: {
    title: '统计分析',
    icon: 'side-statistics'
  },
  children: [
    {
      path: 'devices',
      component: () => import('@/views/statistics/devices/index'), // Parent router-view
      name: 'Devices',
      meta: { title: '充电设备统计' }
    },
    {
      path: 'port',
      component: () => import('@/views/statistics/port/index'),
      name: 'Port',
      meta: { title: '充电端口使用率统计' }
    },
    {
      path: 'charge',
      component: () => import('@/views/statistics/charge/index'), // Parent router-view
      name: 'Charge',
      meta: { title: '服务费收费标准统计' }
    },
    {
      path: 'user',
      component: () => import('@/views/statistics/user/index'),
      name: 'User',
      meta: { title: '用户信息统计' }
    }
  ]
}
