/* Layout */
import Common from '@/layout/common'

export const TagsRoutes = {
  path: '/tags-manage',
  component: Common,
  redirect: '/tags-manage/index',
  children: [{
    path: 'index',
    name: 'TagsManage',
    component: () => import('@/views/tags-manage/index'),
    meta: { title: '标签管理', icon: 'tag-manage', affix: true }
  }]
}
