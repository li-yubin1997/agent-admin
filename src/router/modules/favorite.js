/* Layout */
import Common from '@/layout/common'

export const FavoriteRoutes = {
  path: '/favorite',
  component: Common,
  redirect: '/favorite/index',
  hidden: true,
  children: [{
    path: 'index',
    name: 'Favorite',
    component: () => import('@/views/favorite/index'),
    meta: { title: '收藏夹', icon: 'side-account-manage', affix: true }
  }]
}
