/* Layout */
import Common from '@/layout/common'

export const TaskRoutes = {
  path: '/task-manage',
  component: Common,
  redirect: '/task-manage/index',
  children: [{
    path: 'index',
    name: 'TaskManage',
    component: () => import('@/views/task-manage/index'),
    meta: { title: '任务管理', icon: 'side-account-manage', affix: true }
  }]
}
