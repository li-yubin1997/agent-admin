/* Layout */
import Common from '@/layout/common'

export const OperatorRoutes = {
  path: '/operator',
  component: Common,
  redirect: '/operator/index',
  children: [{
    path: 'index',
    name: 'Operator',
    component: () => import('@/views/operator/index'),
    meta: { title: '运营商管理', icon: 'side-operator', affix: true }
  }]
}
