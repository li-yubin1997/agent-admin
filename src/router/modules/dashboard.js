export const DashboardRoutes = {
  path: '/dashboard',
  children: [{
    path: 'index',
    name: 'Dashboard',
    component: () => import('@/views/dashboard/index'),
    meta: { title: '达人管理', icon: 'side-log', affix: true }
  }]
}
