/* Layout */
import Common from '@/layout/common'

export const UserRoutes = {
  path: '/user-info',
  component: Common,
  redirect: '/user-info/index',
  hidden: true,
  children: [{
    path: 'index',
    name: 'userInfo',
    component: () => import('@/views/user-info/index'),
    meta: { title: '用户信息', icon: 'side-account-manage', affix: true }
  }]
}
