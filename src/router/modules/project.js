/* Layout */
import Common from "@/layout/common";
// import Layout from '@/layout'
export const ProjectRoutes = {
  path: "/project-manage",
  component: Common,
  redirect: "/project-manage/index",
  children: [
    {
      path: "index",
      name: "ProjectManage",
      component: () => import("@/views/project-manage/index"),
      meta: { title: "计划管理", icon: "project-manage", affix: true },
    },
  ],
};

export const AddProjectRoutes = {
  path: "/add-project",
  component: Common,
  redirect: "/add-project/index",
  hidden: true,
  children: [
    {
      path: "index",
      name: "AddProject",
      component: () => import("@/views/add-project/index"),
      meta: { title: "新增计划", icon: "side-account-manage", affix: true },
    },
  ],
};

export const AddNetCelebrityRoutes = {
  path: "/add-net-celebrity",
  component: Common,
  redirect: "/add-net-celebrity/index",
  hidden: true,
  children: [
    {
      path: "index",
      name: "AddNetCelebrity",
      component: () => import("@/views/add-net-celebrity/index"),
      meta: { title: "新增达人", icon: "side-account-manage", affix: true },
    },
  ],
};

export const DetailProjectRoutes = {
  path: "/detail-project",
  component: Common,
  redirect: "/detail-project/index",
  hidden: true,
  children: [
    {
      path: "index",
      name: "DetailProject",
      component: () => import("@/views/detail-project/index"),
      meta: { title: "计划详情", icon: "side-account-manage", affix: true },
    },
  ],
};

export const CooperativeListRoutes = {
  path: "/cooperative-list",
  component: Common,
  redirect: "/cooperative-list/index",
  hidden: true,
  children: [
    {
      path: "index",
      name: "CooperativeList",
      component: () => import("@/views/cooperative-list/index"),
      meta: { title: "想要合作达人", icon: "side-account-manage", affix: true },
    },
  ],
};
