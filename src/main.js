import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import '@/styles/scrollbar.css'
import 'element-ui/lib/theme-chalk/index.css'
import { message } from './utils/reset-message' // reset message
import '@/styles/index.scss' // global css
import App from './App'
import store from './store'
import router from './router'

// 导入配置好的国际化语言包
import i18n from './i18n' // Internationalization

import '@/icons' // icon
import '@/permission' // permission control
// import china from 'echarts/map/json/china.json'
// echarts.registerMap('china', china)
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

import VueClipboard from 'vue-clipboard2';
Vue.use(VueClipboard)

// ElementUI
ElementUI.Dialog.props.closeOnClickModal.default = false
// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(ElementUI, {
  size: 'medium', // set element-ui default size设置元素默认大小
  i18n: (key, value) => i18n.t(key, value), // 在注册Element时设置i18n的处理方法
})
Vue.prototype.$message = message

Vue.config.productionTip = false
/**
 * 注册全局日期过滤器
 * @params { formatStr } 时间格式
 * @params { date } 任意时间形式
 */
import moment from 'moment'
Vue.filter('formatDate', function (date, formatStr = 'YYYY-MM-DD HH:mm:ss') {
  return date ? moment(date).format(formatStr) : ''
})

/**
 * 全局混用
 */
Vue.mixin({
  computed: {
    tagNameLang() {
      return (key) => {
        return i18n.locale === 'en' ? key + 'En' : key
      }
    },
  },
  methods: {
    // 表格连续序号
    tableIndex(index, page, pageSize) {
      if (typeof page === 'number' && typeof pageSize === 'number') {
        return (page - 1) * pageSize + index + 1
      } else {
        return index + 1
      }
    },
  },
})

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: (h) => h(App),
})
