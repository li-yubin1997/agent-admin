import request from '@/utils/request'

// 查
export function onload(data) {
  return request({
    url: '/nca/ad-project/page',
    method: 'post',
    data
  })
}

// 改
export function update(data) {
  return request({
    url: '/nca/ad-project/update',
    method: 'post',
    data
  })
}

// 删
export function del(data) {
  return request({
    url: '/nca/ad-project/del',
    method: 'post',
    data
  })
}

// 增
export function add(data) {
  return request({
    url: '/nca/ad-project/add',
    method: 'post',
    data
  })
}
