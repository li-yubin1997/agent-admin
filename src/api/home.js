import request from '@/utils/request'
// 获取区域统计数据
export function queryAreaStatistic(data) {
  return request({
    url: '/statistic/area/query',
    method: 'post',
    data
  })
}
// 获取基础统计数据
export function queryBaseStatistic(data) {
  return request({
    url: '/statistic/base/query',
    method: 'post',
    data
  })
}

// 获取充电设备统计设计
export function queryDeviceStatistic(data) {
  return request({
    url: '/statistic/device/query',
    method: 'post',
    data
  })
}

// 获取用户数量统计
export function queryUserStatistic(data) {
  return request({
    url: '/statistic/user/query',
    method: 'post',
    data
  })
}
// 获取使用率统计
export function queryUseRateStatistic(data) {
  return request({
    url: '/statistic/useRate/query',
    method: 'post',
    data
  })
}
