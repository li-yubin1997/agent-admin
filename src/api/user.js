import request from "@/utils/request";
import store from "@/store";

export function login(data) {
  return request({
    url: "/nca/broker-user/login",
    method: "post",
    data,
  });
}

export function logout() {
  return request({
    url: "/nca/broker-user/logout",
    method: "post",
    data: {
      id: store.getters.userInfo.id,
    },
  });
}

// 免登录
export function loginFree(id) {
  return request({
    url: "/nca/admin-user/switch/broker-user",
    method: "post",
    data: {
      id,
    },
  });
}
