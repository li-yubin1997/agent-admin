import request from '@/utils/request'

// 上报日志列表
export function uploadLogList(data) {
  return request({
    url: '/log/merchants/page/query',
    method: 'post',
    data
  })
}

// 登录日志
export function loginLogList(data) {
  return request({
    url: '/log/login/page/query',
    method: 'post',
    data
  })
}
