import request from '@/utils/request'

// 运营商列表
export function operatorList(data) {
  return request({
    url: '/merchants/page/query',
    method: 'post',
    data
  })
}

// 删除运营商账户
export function deleteOperator(data) {
  return request({
    url: '/merchants/del',
    method: 'post',
    data
  })
}

// 修改运营商
export function updateOperator(data) {
  return request({
    url: '/merchants/update',
    method: 'post',
    data
  })
}

// 新增运营商
export function addOperator(data) {
  return request({
    url: '/merchants/add',
    method: 'post',
    data
  })
}
