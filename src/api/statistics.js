import request from '@/utils/request'

// 充电桩地区列表查询
export function areaTree(data) {
  return request({
    url: '/pileInfo/condition/query',
    method: 'post',
    data
  })
}

// 充电设备统计
export function devicesList(data) {
  return request({
    url: '/device/statis',
    method: 'post',
    data
  })
}

// 充电设备统计导出
export function devicesListDown(data) {
  return request({
    url: '/device/down',
    method: 'post',
    data
  })
}

// 充电端口使用率统计
export function portList(data) {
  return request({
    url: '/pileUseRate/query',
    method: 'post',
    data
  })
}

// 充电端口使用率统计导出
export function portListDown(data) {
  return request({
    url: '/pileUseRate/down',
    method: 'post',
    data
  })
}

// 服务费收费标准统计
export function serviceChargeList(data) {
  return request({
    url: '/pileInfo/page/query',
    method: 'post',
    data
  })
}

// 服务费收费标准统计导出
export function serviceChargeListDown(data) {
  return request({
    url: '/pileInfo/down',
    method: 'post',
    data
  })
}

// 用户信息统计
export function userInfoList(data) {
  return request({
    url: '/pileUser/query',
    method: 'post',
    data
  })
}

// 用户信息统计导出
export function userInfoListDown(data) {
  return request({
    url: '/pileUser/down',
    method: 'post',
    data
  })
}
