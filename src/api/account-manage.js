import request from '@/utils/request'

// 账户列表
export function accountList(data) {
  return request({
    url: '/backStageUser/page/query',
    method: 'post',
    data
  })
}

// 修改账号
export function updateAccount(data) {
  return request({
    url: '/backStageUser/update',
    method: 'post',
    data
  })
}

// 删除账户
export function deleteAccount(data) {
  return request({
    url: '/backStageUser/del',
    method: 'post',
    data
  })
}

// 新增账户
export function addAccount(data) {
  return request({
    url: '/backStageUser/add',
    method: 'post',
    data
  })
}
