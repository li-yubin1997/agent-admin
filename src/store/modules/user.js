import { login, logout, loginFree } from "@/api/user";
import {
  getToken,
  setToken,
  removeToken,
  setUserData,
  getUserData,
  removeUserData,
} from "@/utils/auth";
import { resetRouter } from "@/router";
// import { sha256_digest } from '@/utils/sha256'

const getDefaultState = () => {
  return {
    token: getToken(),
    userInfo: getUserData(),
    roles: [],
  };
};

const state = getDefaultState();

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState());
  },
  SET_TOKEN: (state, token) => {
    state.token = token;
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles;
  },
  SET_USERINFO: (state, userInfo) => {
    state.userInfo = userInfo;
  },
};

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo;
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password })
        .then((response) => {
          console.log(response.headers);
          const { data } = response;
          commit("SET_TOKEN", data.token);
          commit("SET_USERINFO", data);
          commit("SET_ROLES", data.roles || []);
          setToken(data.token);
          setUserData(data);
          resolve(data);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token)
        .then(() => {
          removeToken(); // must remove  token  first
          removeUserData();
          resetRouter();

          commit("RESET_STATE");
          commit("SET_ROLES", []);
          resolve();
        })
        .catch((error) => {
          reject(error);
        });
    });
  },

  loginFree({ commit }, id) {
    return new Promise(async (resolve, reject) => {
      const response = await loginFree(id);
      const { data } = response;
      commit("SET_TOKEN", data.token);
      commit("SET_USERINFO", data);
      commit("SET_ROLES", data.roles || []);
      setToken(data.token);
      setUserData(data);
      resolve(data);
    }).catch((error) => {
      reject(error);
    });
  },

  // remove token
  resetToken({ commit }) {
    return new Promise((resolve) => {
      removeToken(); // must remove  token  first
      commit("RESET_STATE");
      resolve();
    });
  },

  setRoles({ commit }, roles) {
    commit("SET_ROLES", roles);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
