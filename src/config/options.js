// 账户状态
export const accountStatus = [
  { value: 1, label: '启用', setting: '停用' },
  { value: 2, label: '停用', setting: '启用' },
]

// 运营商数据上报状态
export const operatorDataStatus = [
  { value: 1, label: '正常', setting: '数据停用' },
  { value: 0, label: '停用', setting: '数据启用' },
]

// 登录类型
export const loginWay = [
  { value: 1, label: '登录' },
  { value: 2, label: '登出' },
]

// 运营商上报数据类型
export const operatorReportType = [
  { value: 1, label: '充电桩信息' },
  { value: 2, label: '充电记录' },
  { value: 3, label: '用户信息' },
]

// 粉丝数量级
export const funsNumsList = [
  {
    id: 1,
    name: '<10K',
    min: 0,
    max: 10000,
  },
  {
    id: 2,
    name: '10k-50k',
    min: 10000,
    max: 50000,
  },
  {
    id: 3,
    name: '50k-100k',
    min: 50000,
    max: 100000,
  },
  {
    id: 4,
    name: '100k-500k',
    min: 100000,
    max: 500000,
  },
  {
    id: 5,
    name: '500k-1M',
    min: 500000,
    max: 1000000,
  },
  {
    id: 6,
    name: '1M-2M',
    min: 1000000,
    max: 2000000,
  },
  {
    id: 7,
    name: '2M-3M',
    min: 2000000,
    max: 3000000,
  },
  {
    id: 8,
    name: '3M-4M',
    min: 3000000,
    max: 4000000,
  },
  {
    id: 9,
    name: '4M-5M',
    min: 4000000,
    max: 5000000,
  },
  {
    id: 10,
    name: '5M-10M',
    min: 5000000,
    max: 10000000,
  },
  {
    id: 11,
    name: '10M-20M',
    min: 10000000,
    max: 20000000,
  },
  {
    id: 12,
    name: '20M+',
    min: 20000000,
    // max: ,
  },
]
export const sexRatioList = [
  {
    id: 1,
    name: '男90%-女10%',
    man: 90,
    woman: 10,
  },
  {
    id: 2,
    name: '男80%-女20%',
    man: 80,
    woman: 20,
  },
  {
    id: 3,
    name: '男70%-女30%',
    man: 70,
    woman: 30,
  },
  {
    id: 4,
    name: '男60%-女40%',
    man: 60,
    woman: 40,
  },
  {
    id: 5,
    name: '男50%-女50%',
    man: 50,
    woman: 50,
  },
  {
    id: 6,
    name: '男40%-女60%',
    man: 40,
    woman: 60,
  },
  {
    id: 7,
    name: '男30%-女70%',
    man: 30,
    woman: 70,
  },
  {
    id: 8,
    name: '男20%-女80%',
    man: 20,
    woman: 80,
  },
  {
    id: 9,
    name: '男10%-女90%',
    man: 10,
    woman: 90,
  },
]

export const agesList = [
  {
    id: 1,
    name: '<18',
    min: 0,
    max: 18,
  },
  {
    id: 2,
    name: '18-24',
    min: 18,
    max: 24,
  },
  {
    id: 3,
    name: '25-34',
    min: 25,
    max: 34,
  },
  {
    id: 4,
    name: '35-44',
    min: 35,
    max: 40,
  },
  {
    id: 5,
    name: '45-54',
    min: 45,
    max: 54,
  },
  {
    id: 6,
    name: '55-64',
    min: 55,
    max: 64,
  },
  {
    id: 7,
    name: '>55',
    min: 55,
    max: 200,
  },
]

// 平台类型列表 quotationList-报价形式
export const platformList = [
  {
    id: 1,
    name: 'Tik Tok',
    imgUrl: require('@/img/tiktok.png'),
    quotationList: [{ name: 'Livestream' }, { name: 'Tiktok Video' }],
  },
  {
    id: 2,
    name: 'You Tube',
    imgUrl: require('@/img/YouTube.png'),
    quotationList: [
      { name: 'pre-role' },
      { name: 'Full Video' },
      { name: 'Livestream' },
      { name: 'Text & image' },
    ],
  },
  {
    id: 3,
    name: 'Instagram',
    imgUrl: require('@/img/Instagram.png'),
    quotationList: [
      { name: 'LG Post(Text&Image)' },
      { name: 'Story Highlight' },
      { name: 'Reels' },
      { name: 'LGTV' },
    ],
  },
  {
    id: 4,
    name: 'Facebook',
    imgUrl: require('@/img/Facebook.png'),
    quotationList: [
      { name: 'pre-role' },
      { name: 'Full Video' },
      { name: 'Livestream' },
      { name: 'Text & image' },
    ],
  },
  {
    id: 5,
    name: 'Twitter',
    imgUrl: require('@/img/Twitter.png'),
    quotationList: [
      { name: 'Text & image' },
      { name: 'Twitter Thread(max 3 tweet+with 1 Photo)' },
      { name: 'Text Only' },
    ],
  },
  {
    id: 6,
    name: 'Twitch',
    imgUrl: require('@/img/Twitch.png'),
    quotationList: [{ name: 'Livestream' }],
  },
  {
    id: 7,
    name: 'Other',
    imgUrl: require('@/img/other.png'),
    quotationList: [
      { name: 'pre-role' },
      { name: 'Full Video' },
      { name: 'Livestream' },
      { name: 'Text & image' },
    ],
  },
]

// 行业选项
export const industryOptions = [
  '游戏',
  '电商',
  'APP',
  '广告代理商',
  'web3/数字货币相关',
]

// 国家地区
export const cityOptions = [
  { value: '东南亚-SEA' },
  { value: '马来西亚-MY' },
  { value: '印度尼西亚-ID' },
  { value: '加拿大-CA' },
  { value: '美国-US' },
  { value: '德国-DE' },
  { value: '法国-FR' },
  { value: '英国-UK' },
  { value: '日本-JP' },
  { value: '韩国-KR' },
  { value: '泰国-TH' },
  { value: '越南-VN' },
  { value: '菲律宾-PH' },
  { value: '英语区-ENG' },
  { value: '南美洲-LT' },
  { value: '中东非-MENA' },
  { value: '阿联酋-UAE' },
  { value: '巴西-BR' },
  { value: '哥伦比亚-GL' },
  { value: '墨西哥-MX' },
  { value: '葡萄牙-PT' },
  { value: '秘鲁-PE' },
  { value: '阿根廷-AR' },
  { value: '印度-IN' },
  { value: '澳大利亚-AU' },
  { value: '丹麦-DK' },
  { value: '西班牙-ES' },
  { value: '芬兰-FI' },
  { value: '希腊-GR' },
  { value: '香港-HK' },
  { value: '意大利-IT' },
  { value: '柬埔寨-KH' },
  { value: '缅甸-MM' },
  { value: '澳门-MO' },
  { value: '荷兰-NL' },
  { value: '挪威-NO' },
  { value: '新西兰-NZ' },
  { value: '台湾-TW' },
  { value: '南非-ZA' },
]
