# 开发须知

## 相关文档和地址

```
// 服务器接口文档
http://10.100.12.100:8666/project/38/interface/api

// 测试环境
http://10.216.192.219/regulatory/

// 正式环境

```

## 构建和打包

```
// 运行测试环境
npm run dev

// 运行正式环境
npm run prod

// 测试环境打包
npm run build:dev

// 正式环境打包
npm run build:prod
```

 

本项目测试环境和正式环境均在二级域名 ```http://www.xxx.com/regulatory/```下，如需更改为一级路由或者二级路由名称则在

```
// .env.production || .env.development 修改 VUE_APP_PACKAGE
# base app root
VUE_APP_PACKAGE = '/regulatory/'
```





# 框架说明

本框架基于 vue-element-admin 进行一些优化和改动，更多信息请参考 [使用文档](https://panjiachen.github.io/vue-element-admin-site/zh/)。本文档主要用于让新接触此框架的开发，可以根据规范迅速进行业务开发。

# 基础功能

## 权限判断

vue-element-admin 的权限控制主要通过登录人的角色身份`roles`，系统中主要分为了路由权限和颗粒化权限（页面按钮）。用户身份可以根据业务需求换成不同的称号或者代号。例如：

- 可以分为超级管理员、管理员、运营者等等
- 可以分为不同运营商、代理商等等

`roles` 权限为**数组**，支持一个用户有多种角色。

### 路由权限

需要判断权限的路由放在`@/src/router/index.js` 的`asyncRoutes` 数组中，访问权限通过在`meta` 中设置 `roles` ，例如

```javascript
{
  path: '/example',
  component: Layout,
  redirect: '/example/table',
  name: 'Example',
  meta: { title: 'Example', icon: 'el-icon-s-help' },
  children: [
    {
      path: 'table',
      name: 'Table',
      component: () => import('@/views/table/index'),
      meta: { title: 'Table', icon: 'table', roles: ['admin', 'editor'] }
    },
    {
      path: 'tree',
      name: 'Tree',
      component: () => import('@/views/tree/index'),
      meta: { title: 'Tree', icon: 'tree', roles: ['admin'] }
    }
  ]
}
```

### 颗粒化权限

#### 自定义指令

暂定为按需引入，如果项目要求需要经常前端判断权限可以改为全局注册

```
// template
<h1 v-permission="['editor']">Editor</h1>
```

```javascript
// script
import permission from '@/directive/permission/index.js' // 权限判断指令

export default {
  directives: { permission },
}
```

#### 权限判断方法

有一些自定义指令使用不到的地方可以直接调用函数判断权限

```javascript
import checkPermission from '@/utils/permission' // 权限判断函数

export default {
	methods: {
    	checkPermission,
    }
}
```



## mock

启动一个`mock-server`来模拟数据，如果不使用可直接注释 `vue.config.js` 中 `devServer` 的 `before`即可。



### 新增和修改

如果你想添加 mock 数据，只要在根目录下找到`mock`文件，添加对应的路由，对其进行拦截和模拟数据即可。可以参考`mock`文件夹中`tabel.js`。

1. 在`@/api`下新建 api 接口
2. 在`@/mock`下 api 对应的 js 文件中进行拦截，注意接口名称和方法与 api 一致



当后台接口开发完成后，删除对应mock即可。



# 业务开发

## 新增页面

```@/views``` 下面文件夹由侧边栏菜单和其余页面组成。

新增菜单或者子菜单

1. 在对应菜单文件下新建菜单文件夹，在此菜单新建```index.vue``` 和```components``` 文件夹（存放该菜单下的组件）
2. 在对应菜单路由文件下添加路由（[路由配置点击查看](https://panjiachen.github.io/vue-element-admin-site/zh/guide/essentials/router-and-nav.html)）
3. 嵌套路由需要在上一级路由根文件下添加```<router-view>```



## 新增组件

在 ```@/components``` 下写全局的组件，如富文本，各种搜索组件，封装的日期组件表格组件等等能被公用的组件。每个页面或者模块特定的业务组件写在```@/views/当前模块/components/ ```下。 



## 新增API

在```@/api/``` 对应模块的js文件中创建api服务，将views和api两个模块一一对应，方便维护。

对于全区公用的api，放置在```@/api/comment.js``` 中

```javascript
// api/article.js
import request from '../utils/request';
export function fetchList(query) {
  return request({
    url: '/article/list',
    method: 'get',
    params: query
  })
}


// views/example/list
import { fetchList } from '@/api/article'
export default {
  data() {
    list: null,
    listLoading: true
  },
  methods: {
    fetchData() {
      this.listLoading = true
      fetchList().then(response => {
        this.list = response.data.items
        this.listLoading = false
      })
    }
  }
}
```



# 开发规范

## 文件命名规范

### Component

所有`Component`文件都以**大写开头**（大驼峰），除了`index.vue`

- `@/components/BackToTop/index.vue`
- `@/components/Charts/Line.vue`
- `@/views/example/components/Button.vue`



### JS 文件

所有的`.js`文件都遵循**横线连接** (kebab-case)

- `@/utils/open-window.js`
- `@/views/svg-icons/require-icons.js`
- `@/components/MarkdownEditor/default-options.js`



### Views

在`views`文件下，代表路由的`.vue`文件都使用**横线连接** (kebab-case)，代表路由的文件夹也是使用同样的规则。

- `@/views/svg-icons/index.vue`
- `@/views/svg-icons/require-icons.js`



## Eslint

1. 安装 eslint 插件

2. 设置vscode

   注意随着vscode版本更新和插件更新，配置项可能会有所变更，自行修改

   ```json
   {
     "files.autoSave": "off",
     "eslint.validate": ["javascript", "html", "vue"],
     "eslint.run": "onSave",
     "editor.codeActionsOnSave": {
       "source.fixAll.eslint": true
     }
   }
   ```

   

团队成员先在自己的编辑器中配置 `eslint`，在 webpack 中配置并开启 `eslint-loader` 错误提示，这样平时写的时候编辑器就能帮你修正一些简单的格式错误，同时提醒你有哪些不符合 `lint` 规范的的地方，并在命令行中提示你错误。这方面详细的内容见 [ESLint](https://panjiachen.github.io/vue-element-admin-site/zh/guide/advanced/eslint.html)。



```
// 自行修复一些简单的错误
npm run lint -- --fix
```



使用`lint-staged`，它会在你本地 `commit` 之前，校验你提交的内容是否符合你本地配置的 `eslint`规则(这个见文档 [ESLint](https://panjiachen.github.io/vue-element-admin-site/zh/guide/advanced/eslint.html) )，如果符合规则，则会提交成功。如果不符合它会自动执行 `eslint --fix` 尝试帮你自动修复，如果修复成功则会帮你把修复好的代码提交，如果失败，则会提示你错误，让你修好这个错误之后才能允许你提交代码。
